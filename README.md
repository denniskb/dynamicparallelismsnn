## Dynamic parallelism for synaptic updating in GPU-accelerated spiking neural network simulations

CUDA implementation of randomly connected pulse-coupled Izhikevich neurons (Izhikevich, 2003).
Dynamic parallelism is used at the synaptic update step in the spiking neural network simulations.

Code is developed under Ubuntu with Tesla K40, CUDA Toolkit 8.0.

Note that dynamic parallelism requires compute capability 3.5 and separate compilation.

### Installation

Clone the repository go to the folder and run 'make'. This will generate an executable 'dynamic_sim'

### Run

Run the executable by number of neurons and number of synapses per neuron:

```
#!bash

./dynamic_sim N_neurons N_synapses
```

The code will run the same network with 3 parallelization strategies: N-, S- and AP-algorithms; and 3 neural states for each: quiet, balanced and irregular. Simulation results for a network with 2500 neurons and 1000 synapses per each neuron will be written into folders:

```
#!bash
/Results/N2500Nsyn1000Regime0Alg0
/Results/N2500Nsyn1000Regime0Alg1
/Results/N2500Nsyn1000Regime0Alg2
...
```

Regime: 0 - quiet, 1 - balanced, 2 - irregular;
Alg: 0 - AP-algorithm, 1 - N-algorithm, 2 - S-algorithm

### Output files

``sim_overview.csv`` - simulation overview
```
N elapsed(ms) elapsedwithdata(ms) totalspkcount
```

``spiketimes.csv`` - simulation results
```
SpkTime NeuronID
```

``neuroninfo.csv`` - ``neuroninfoafter.csv`` - neural parameters and variables of each neuron before and after simulation.
```
a b c d v u I id nospks
```
``computetime.csv`` - execution times of kernel update and synaptic update at each time step.
```
TimeStep SpksPerStep TimeKernelUpdate TimeSpent
```