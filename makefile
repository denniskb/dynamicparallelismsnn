ARCH="arch=compute_35,code=sm_35"

dynamic_sim: dynamic_sim.o

dynamic_sim.o: dynamic_sim.cu
	nvcc -G -g -O0 -gencode $(ARCH) -odir "." -M -o "$(@:%.o=%.d)" "$<"
	nvcc -G -g -O0 --compile --relocatable-device-code=true -gencode $(ARCH) -x cu -o  "$@" "$<"

dynamic_sim: dynamic_sim.o
	nvcc --cudart static --relocatable-device-code=true -gencode $(ARCH) -link -o  dynamic_sim  dynamic_sim.o

all: dynamic_sim

clean:
	rm *.o *.d dynamic_sim

rmdata:
	rm -rf Results
